#ifndef clique_HPP
#define clique_HPP

using namespace std;

#include "./util/grafo.hpp"

class Clique {
    Grafo *g;
public:
    Clique(char name[]);
    ~Clique();
    void clique();
    void info();
};

#endif