#include "./grafo.hpp"

Grafo::Grafo (char* name) {
    FILE *arq;

    if((arq = fopen(name, "r")) == NULL) {
        std::cout << "Falha ao abrir o arquivo" << std::endl;
        exit(1);
    }
    // pegando informacoes sobre o grafo
    fscanf(arq, "%d", &n);

    // alocando matriz
    matriz = new int*[n];
    for(unsigned int i = 0; i < n; i++)
        matriz[i] = new int[n];

    // insere arestas na matriz
    for(unsigned i = 0; i < n; i++){ 
        for(unsigned j = 0; j < n; j++)
            fscanf(arq, "%d", &matriz[i][j]);
    }

    melhor = new int[n];

    fclose(arq);
}

Grafo::Grafo() {
    melhor = NULL;
    matriz = NULL;
    tamMelhor = 0;
    n = 0;
}

Grafo::~Grafo() {
    if(matriz != NULL) {
        for(unsigned int i = 0; i < n; i++)
            delete matriz[i];
        delete matriz;
    }
    if(melhor != NULL)
        delete melhor;
}

void Grafo::print () {
    for(unsigned i = 0; i < n; i++){ 
        for(unsigned j = 0; j < n; j++)
            std::cout << matriz[i][j] << " ";
        std::cout << std::endl;
    }
}

bool Grafo::aresta(unsigned int a, unsigned int b) {
    return matriz[a][b] == 1 ? true : false;
}

int Grafo::getN() {
    return n;
}

bool Grafo::fazParteDoClique(int *vet, int v) {
    for(int i = 0; i <= v; i++) {
        if(vet[i] != 0) { // o vertice faz parte do clique
            if(!this->aresta(i, v)) // o vertice nao tem aresta com 'v' 
                return false;
        }
    }
    return true;
}

void Grafo::inverterGrafo() {
    for(unsigned i = 0; i < n; i++)
        for(unsigned j = 0; j < n; j++)
            if(i != j)
                    matriz[i][j] = !matriz[i][j];
}

void Grafo::clique (int *vet, int h, int n, int tam) {
    if(h == n + 1)
        return;

    int *vet1, *vet2;

    if(this->fazParteDoClique(vet, h - 1)) { // se o novo verticie for fazer parte do clique, ou seja, o clique almentou
        vet1 = new int[n];
        for(int i = 0; i < n; i++)
            vet1[i] = vet[i];
        vet1[h - 1] = 1;

        if(tam + 1 > tamMelhor) {
            melhor = vet1;
            tamMelhor = tam + 1;
        }
        clique(vet1, h + 1, n, tam + 1);
        if(vet1 != melhor) // se vet1 e melhor nao estao no mesmo endereço de memoria
            delete vet1;
    }
    if(n - h + tam >= tamMelhor) { // se ainda é possível criar um clique maior que o melhor existente
        vet2 = new int[n];

        for(int i = 0; i < n; i++)
            vet2[i] = vet[i]; 
        vet2[h - 1] = 0;

        if(tam > tamMelhor) {
            melhor = vet2;
            tamMelhor = tam;
        }
        clique(vet2, h+1, n, tam);
        if(vet2 != melhor) // se vet2 e melhor nao estao no mesmo endereço de memoria
            delete vet2;
    }

}

void Grafo::clique() {
    for(unsigned i = 0; i < n; i++)
        melhor[i] = 0;
    tamMelhor = 0;

    this->clique(melhor, 1, n, 0);
}

void Grafo::info() {
    std::cout << "numero de vertices: " << tamMelhor << std::endl << "vertices: ";
    for(unsigned int i = 0; i < n; i++)
        std::cout << melhor[i] << " ";
    std::cout << std::endl;
}

void Grafo::conjuntoIndependente() {
    for(unsigned i = 0; i < n; i++)
        melhor[i] = 0;
    tamMelhor = 0;

    this->inverterGrafo();
    this->clique(melhor, 1, n, 0);
}