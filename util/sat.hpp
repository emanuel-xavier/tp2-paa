#ifndef sat_HPP
#define sat_HPP

#include <iostream>

#include "./grafo.hpp"

class Sat : public Grafo {
    unsigned int e, clausulas;
    int **sat;
    bool satisfazivel;

public:
    Sat (char* name);
    ~Sat ();
    void print ();
    void clique (int *vet, int h, int n, int tam);
    bool satisfabilidade ();
    bool getsatisfazivel ();
    void combinacao ();
};

#endif