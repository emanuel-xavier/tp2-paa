#include "./sat.hpp"

Sat::Sat (char* name) {
    FILE *arq;

    satisfazivel = false;

    if((arq = fopen(name, "r")) == NULL) {
        std::cout << "Falha ao abrir o arquivo" << std::endl;
        exit(1);
    }
    // pegando informacoes sobre quantas variáveis
    fscanf(arq, "%d", &e);

    char *linha = new char[3 * e + 3];

    //conta linhas
    clausulas = -1;
    while(!feof(arq)) {
        fgets(linha, 3 * e + 3, arq);
        clausulas++;
    }

    fclose(arq);

    if((arq = fopen(name, "r")) == NULL) {
        std::cout << "Falha ao abrir o arquivo" << std::endl;
        exit(1);
    }
    fscanf(arq, "%d", &e);

    //alocando matriz
    sat = new int*[clausulas];
    for(unsigned int i = 0; i < clausulas; i++) {
        sat[i] = new int[e];
        for(unsigned int j = 0; j < e; j++) {
            fscanf(arq, "%d", &sat[i][j]);
        }
    }

    n = clausulas * e;
    int *vet = new int[n];
    for(unsigned int i = 0; i < n; i++)
        vet[i] = 0;
    melhor = vet;
    matriz = NULL;
    tamMelhor = 0;
    
    matriz = new int*[clausulas * e];
    for(unsigned int i = 0; i < clausulas * e; i++)
        matriz[i] = new int[clausulas * e];

    for(unsigned int i = 0; i < clausulas; i++) {
        for(unsigned int k = 0; k < e; k++) {
            for(unsigned int j = 0; j < clausulas; j++) {
                for(unsigned int l = 0; l < e; l++) {
                    if(i == j || (sat[i][k] == -1 || sat[j][l] == -1)) // mesma clausula || uma dekas nao pertence a a clausula
                        matriz[i * e + k][j * e + l] = 0;
                    else {
                        if(k == l) { // mesma variável em conjuntos diferentes
                            if((sat[i][k] == 0 && sat[j][l] == 1) || (sat[i][k] == 1 && sat[j][l] == 0)) // uma variável é negada e a outra nao
                                matriz[i * e + k][j * e + l] = 0;
                            else {
                                matriz[i * e + k][j * e + l] = 1;
                            }
                        }
                        else // mesma variável em conjuntos diferentes
                            matriz[i * e + k][j * e + l] = 1;
                    }
                }
            }
        }
    }

    fclose(arq);   
}

Sat::~Sat() {
    for(unsigned int i = 0; i < clausulas; i++)
        delete sat[i];

    delete sat;
}

void Sat::print () {
    for(unsigned int i = 0; i < n; i++){ 
        for(unsigned int j = 0; j < e; j++) {
            if(matriz[i][j] >= 0)
                std::cout << " " << matriz[i][j] << " ";
            else
                std::cout << matriz[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void Sat::clique (int *vet, int h, int n, int tam) {
    if(h == n + 1 || satisfazivel)
        return;

    int *vet1, *vet2;

    if(this->fazParteDoClique(vet, h - 1)) { // se o novo verticie for fazer parte do clique, ou seja, o clique almentou
        vet1 = new int[n];
        for(int i = 0; i < n; i++)
            vet1[i] = vet[i];
        vet1[h - 1] = 1;

        if(tam + 1 > tamMelhor) {
            melhor = vet1;
            tamMelhor = tam + 1;
        }

        if(tam + 1 == (int)clausulas) { // se foi encontrado um clique do que o seu tamanho é igual ao número de clausulas 
            satisfazivel = true;
            return;
        }
        
        clique(vet1, h + 1, n, tam + 1);
        if(vet1 != melhor) // se vet1 e melhor nao estao no mesmo endereço de memoria
            delete vet1;
    }
    if(n - h + tam >= tamMelhor) { // se ainda é possível criar um clique maior que o melhor existente
        vet2 = new int[n];

        for(int i = 0; i < n; i++)
            vet2[i] = vet[i]; 
        vet2[h - 1] = 0;

        if(tam > tamMelhor) {
            melhor = vet2;
            tamMelhor = tam;
        }
        clique(vet2, h+1, n, tam);
        if(vet2 != melhor) // se vet2 e melhor nao estao no mesmo endereço de memoria
            delete vet2;
    }
}

bool Sat::satisfabilidade() {
    clique(melhor, 1, n, 0);
    if(satisfazivel) 
        return 1;
    return 0;
}

bool Sat::getsatisfazivel () {
    return satisfazivel;
}

void Sat::combinacao() {

    for(unsigned int i = 0; i < clausulas; i++) {
        std::cout << "(";
        for(unsigned int j = 0; j < e; j++) {
            if(sat[i][j] == 0) 
                std::cout << "x" << j + 1 << " ";
            else if(sat[i][j] == 1) 
                std::cout << "!x" << j + 1 << " ";
        }
        std::cout << ") ";
    }
    std::cout << std::endl << std::endl;

    std::cout << "o valor das variaveis" << std::endl;
    for(unsigned int i = 0; i < clausulas; i++) {
        for(unsigned int j = 0; j < e; j++) {
            if(melhor[i * e + j] == 1) {
                if(sat[i][j] == 0)
                    std::cout << "x";
                else
                    std::cout << "!x";
                std::cout << j + 1 << " da clausula " << i + 1 << std::endl;
            }
        }
    }
    std::cout << "devem ser verdadeiras para que a expressao seja verdadeira" << std::endl;
}