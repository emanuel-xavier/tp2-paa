#include "./independente.hpp"

ConjuntoIndependente::ConjuntoIndependente(char name[]) {
    g = new Grafo(name);
}

ConjuntoIndependente::~ConjuntoIndependente() {
   delete g; 
}  

void ConjuntoIndependente::conjuntoIndependente() {
    g->inverterGrafo();
    g->clique();
}

void ConjuntoIndependente::info() {
    std::cout << "Conjunto independente" << std::endl;
    g->info();
}