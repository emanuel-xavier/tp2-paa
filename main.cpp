#include "./clique.hpp"
#include "./independente.hpp"
#include "./satisfabilidade.hpp"

#include <string.h>

using namespace std;

int main(int argv, char* argc[]) {

    if(argv != 3) {
        cout << "ERRO!  Fomato de entrada inalido" << endl
             << "./exe <metodo> <nome do arquivo>" << endl
             << "metodos:" << endl
             << "*********************************" << endl
             << "*                               *" << endl
             << "* 0: clique maximo              *" << endl
             << "* 1: conjunto independente      *" << endl
             << "* 2: satisfabilidade            *" << endl
             << "*                               *" << endl
             << "*********************************" << endl;
        exit(1);
    }

    if(strcmp(argc[1], "0") == 0) {
        Clique *ptr = new Clique(argc[2]);
        ptr->clique();
        ptr->info();
        delete ptr;
    }
    else if(strcmp(argc[1], "1") == 0) {
        ConjuntoIndependente *ptr = new ConjuntoIndependente(argc[2]);
        ptr->conjuntoIndependente();
        ptr->info();
        delete ptr;
    }
    else if(strcmp(argc[1], "2") == 0) {
        Satisfabilidade *ptr = new Satisfabilidade(argc[2]);
        ptr->satisfabilidade();
        ptr->info();
        delete ptr;
    }
    else
        cout << "Metodo invalido" << endl;

    return 0;
}